<?php

namespace App\Entity;

use App\Repository\ArticlePackRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ArticlePackRepository::class)
 * @ORM\Table(name="articlePack")
 */
class ArticlePack
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity=Article::class, inversedBy="packs", name="article")
     * @ORM\JoinColumn(nullable=false)
     */
    private $article;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity=Pack::class, inversedBy="articles", name="pack")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pack;

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article): self
    {
        $this->article = $article;

        return $this;
    }

    public function getPack(): ?Pack
    {
        return $this->pack;
    }

    public function setPack(?Pack $pack): self
    {
        $this->pack = $pack;

        return $this;
    }
}
