<?php

namespace App\Entity;

use App\Repository\PackRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PackRepository::class)
 * @ORM\Table(name="pack");
 */
class Pack
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="id")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, name="url")
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=255, name="title")
     */
    private $title;

    /**
     * @ORM\Column(type="text", name="content")
     */
    private $content;

    /**
     * @ORM\Column(type="datetime", name="createdAt")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="float", nullable=true, name="price")
     */
    private $price;

    /**
     * @ORM\OneToMany(targetEntity=ArticlePack::class, mappedBy="pack")
     */
    private $articles;

    /**
     * @ORM\OneToMany(targetEntity=VideoPack::class, mappedBy="pack")
     */
    private $videos;

    /**
     * @ORM\OneToMany(targetEntity=UserPack::class, mappedBy="pack")
     */
    private $users;

    public function __construct()
    {
        $this->articles = new ArrayCollection();
        $this->videos = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->createdAt = new DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return Collection|ArticlePack[]
     */
    public function getArticles(): Collection
    {
        return $this->articles;
    }

    public function addArticle(ArticlePack $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
            $article->setPack($this);
        }

        return $this;
    }

    public function removeArticle(ArticlePack $article): self
    {
        if ($this->articles->removeElement($article)) {
            // set the owning side to null (unless already changed)
            if ($article->getPack() === $this) {
                $article->setPack(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|VideoPack[]
     */
    public function getVideos(): Collection
    {
        return $this->videos;
    }

    public function addVideo(VideoPack $video): self
    {
        if (!$this->videos->contains($video)) {
            $this->videos[] = $video;
            $video->setPack($this);
        }

        return $this;
    }

    public function removeVideo(VideoPack $video): self
    {
        if ($this->videos->removeElement($video)) {
            // set the owning side to null (unless already changed)
            if ($video->getPack() === $this) {
                $video->setPack(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserPack[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(UserPack $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setPack($this);
        }

        return $this;
    }

    public function removeUser(UserPack $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getPack() === $this) {
                $user->setPack(null);
            }
        }

        return $this;
    }
}
