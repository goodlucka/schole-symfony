<?php

namespace App\Entity;

use App\Repository\VideoRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VideoRepository::class)
 * @ORM\Table(name="video")
 */
class Video
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name="id")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, name="url")
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=255, name="title")
     */
    private $title;

    /**
     * @ORM\Column(type="text", name="content")
     */
    private $content;

    /**
     * @ORM\Column(type="datetime", name="createdAt")
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity=VideoPack::class, mappedBy="video")
     */
    private $packs;

    public function __construct()
    {
        $this->packs = new ArrayCollection();
        $this->createdAt = new DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @return Collection|VideoPack[]
     */
    public function getPacks(): Collection
    {
        return $this->packs;
    }

    public function addPacks(VideoPack $packs): self
    {
        if (!$this->packs->contains($packs)) {
            $this->packs[] = $packs;
            $packs->setVideo($this);
        }

        return $this;
    }

    public function removePacks(VideoPack $packs): self
    {
        if ($this->packs->removeElement($packs)) {
            // set the owning side to null (unless already changed)
            if ($packs->getVideo() === $this) {
                $packs->setVideo(null);
            }
        }

        return $this;
    }
}
