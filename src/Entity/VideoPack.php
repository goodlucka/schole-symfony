<?php

namespace App\Entity;

use App\Repository\VideoPackRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VideoPackRepository::class)
 * @ORM\Table(name="videoPack")
 */
class VideoPack
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity=Video::class, inversedBy="pack", name="video")
     * @ORM\JoinColumn(nullable=false)
     */
    private $video;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity=Pack::class, inversedBy="videos", name="pack")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pack;

    public function getVideo(): ?Video
    {
        return $this->video;
    }

    public function setVideo(?Video $video): self
    {
        $this->video = $video;

        return $this;
    }

    public function getPack(): ?Pack
    {
        return $this->pack;
    }

    public function setPack(?Pack $pack): self
    {
        $this->pack = $pack;

        return $this;
    }
}
