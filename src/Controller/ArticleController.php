<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\ArticlePack;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    /**
     * @Route("test/{idp}",name="test")
     */
    public function test(int $idp)
    {
        $repository = $this->getDoctrine()->getRepository(ArticlePack::class);
        $articles = $repository->findArticlesInPack($idp);

        return $this->render('test.html.twig',[
            'articles' =>
            array_map(function(Article $article){
                return [
                    'url' => $article->getUrl(),
                    'title' => $article->getTitle(),
                    'packs' => $article->getPacks()
                ];
            },$articles)
        ]);
        /*
        $repository = $this->getDoctrine()->getRepository(Article::class);
        $articles = $repository->findAll();
        $articles = array_map(function(Article $article){
                return [
                    'url' => $article->getUrl(),
                    'title' => $article->getTitle()
                ];
            },$articles);
        return $this->render('test.html.twig',[
            'articles' => $articles
        ]);
        */
    }

    /**
     * @Route("clanek/{url}", name="article")
     */
    public function index(string $url) 
    {
        $repository = $this->getDoctrine()->getRepository(Article::class);
        $article = $repository->findOneBy(['url' => $url]);

        return $this->render('article/index.html.twig',[
            'article' => $article
        ]);
    }
}
