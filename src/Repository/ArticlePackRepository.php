<?php

namespace App\Repository;

use App\Entity\ArticlePack;
use App\Entity\Pack;
use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ArticlePack|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArticlePack|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArticlePack[]    findAll()
 * @method ArticlePack[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticlePackRepository extends ServiceEntityRepository
{
    private $em;
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $em)
    {
        $this->em = $em;
        parent::__construct($registry, ArticlePack::class);
    }


    /**
     * 
     */
    public function findArticlesInPack(int $pack)
    {
        /*
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult(Article::class,'a');
        $rsm->addFieldResult('a','url','url'); 
        $rsm->addFieldResult('a','title','title'); 
        $rsm->addFieldResult('a','content','content'); 
        $rsm->addFieldResult('a','createdAt','created_at'); 

        $query = $this->em->createNamedNativeQuery('
                SELECT 
                    a.url, a.title, a.content, a.created_at
                FROM 
                    article a, article_pack ap, pack p
                WHERE
                    a.id = ap.article_id
                AND
                    ap.pack_id = p.id
                AND 
                    p.id = :pack
            ');
        $query->setParameter('pack',$pack);
        return $query->getResult();*/

        $query = $this->em->createQueryBuilder();
        $query->select('a')
            ->from(Article::class,'a')
            ->innerJoin(ArticlePack::class,'ap')
            ->where('ap.pack = :pack')
            ->setParameter('pack',$pack);
        return $query->getQuery()->execute();
    }

    // /**
    //  * @return ArticlePack[] Returns an array of ArticlePack objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ArticlePack
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
