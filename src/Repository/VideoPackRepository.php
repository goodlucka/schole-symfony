<?php

namespace App\Repository;

use App\Entity\VideoPack;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method VideoPack|null find($id, $lockMode = null, $lockVersion = null)
 * @method VideoPack|null findOneBy(array $criteria, array $orderBy = null)
 * @method VideoPack[]    findAll()
 * @method VideoPack[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VideoPackRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VideoPack::class);
    }

    // /**
    //  * @return VideoPack[] Returns an array of VideoPack objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?VideoPack
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
